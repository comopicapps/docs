# Content Writers Manual

# Login User

1. Goto http://epcstaging.cosango.com/user 
1. Enter username and password.
1. Click login to get login.
1. If you don’t know the username or password please contact your administrator.

# Managing Publication

## Add Publication

1. Login to the site
1. Click Add Content in a top shortcut menu .
1. Click on Publication link.
1. Fill the entire field, require fields with asterisk (*) are compulsory.
1. Enter title of publication.
1. Enter Summary of the publication in the body field.
1. Browse preview and select any of the file with format .pdf, .doc, .docx and file must be less than 20 MB. The preview should be demo version of full publication.
1. Under full Cover image choose file for cover image of publication. They should be with type .png, .gif, .jpg, .jpeg and file must be less than 3 MB.
1. Select publication type, publication type is a required field and you can select only one of any publication type.
1. Similarly select any one of the publication group, it is a required field.
1. Select research unit, it is a required field and you can select only one of any research unit of your publication.
1. Enter No of Pages of publication .
1. Enter No of words of publication .
1. Browse Full document and select any of the file with format .pdf, .doc, .txt and file must be less than 20 MB. The preview should be demo version of full publication.
1. Under Publication QR image choose file for  qr image of publication. It should be with type .png, .gif, .jpg, .jpeg and file must be less than 20 MB
1. Enter ISBN number.
1. Enter Publisher of a publication .
1. Enter EISBN number of a publication .
1. Enter tags for publication .

## Add Publication via Shortcut menu

1. Login to the site .
1. Click Add Publication in a top shortcut menu.
1. Fill the entire fields and require fields with asterik (*) are compulsory in an appeared window.
1. Click Save and publish option in a drop down menu which appeared on clicking an arrow beside save as unpublished in a bottom of a page.  


## Edit Publication

1. Login to the site
1. Click Publication in a top shortcut menu .
1. Scroll down to find the desired publication.
1. Click Edit button Shown at the right corner of each content .
1. Edit fields of a selected publication content.
1. Click Save .

## Delete Publication

1. login to the site.
1. Click Publication in a top shortcut menu .
1. Scroll down to find the desired publication.
1. Click drop down arrow beside Edit button.
1. Click Delete in drop down menu.
1. Confirmation page will occur click on delete will permanently delete the content.
1. Click on Cancel button will not delete the publication .
1. You can also delete from the edit page, delete option is shown at the bottom of the edit page.

## Create draft publication in EPC

1. login to the site.
1. Click Add content in a top shortcut menu.
1. Click on Publication link.
1. Click drop down arrow beside Save and keep published button in the bottom of a page.
1. The draft publication will be created by selecting Save and unpublished option in a drop down menu . 

## Set publication date in EPC

1. login to the site.
1. Click Add content in the top shortcut menu.
1. Click on Publication link.
1. Click Authoring Information tab shown at the right side of the page and write date in Authored on field. The date format is YYYY-MM-DD and +0500 is the time zone offset from UTC. Leave blank to use the time of form submission.


## Set publication author in EPC

1.	login to the site.
1.	Click  Add Content in the top shortcut menu..
1.	Click on Publication link.
1.	Click Authoring Information tab shown at the right side of the page and fill Authored By field (Leave blank for Anonymous).

## Translate a publication

1. Login to the site.
1. Click Publications in top shortcut menu.
1. Scroll down to find the desired publicaiton.
1. Click drop down arrow beside Edit button.
1. Click Translate in drop down menu.
1. Enter the translation of title, body and other required fields.
1. Click Save.

# Managing News

## Add News:

1. Login to the site
1. Click Add Content in a top shortcut menu .
1. Click on News link.
1. Fill the entire field, require fields with asterisk (*) are compulsory.
1. Enter title of news.
1. Enter Summary of the news in the body field.
1. Browse news images choose file of news. They should be with type .png, .gif, .jpg, .jpeg and file must be less than 3 MB.
1. Select Channel from a drop down in a channel field .
1. Select research project from a drop down in the research project field.
1. Select event from a drop down in a event field.

## Add News via Shortcut menu

1. Login to the site .
1. Click Add News in a top shortcut menu.
1. Fill the entire fields and require fields with asterik (*) are compulsory in an appeared window.
1. Click Save and publish option in a drop down menu which appeared on clicking an arrow beside save as unpublished in a bottom of a page.  


## Edit News:

1. Login to the site
1. Click News in a top shortcut menu .
1. Scroll down to find the desired news.
1. Click Edit button Shown at the right corner of each content .
1. Edit fields of a selected news content.
1. Click Save .

## Delete News:

1. login to the site.
1. Click News in a top shortcut menu .
1. Scroll down to find the desired News.
1. Click drop down arrow beside Edit button.
1. Click Delete in drop down menu.
1. Confirmation page will occur click on delete button will permanently delete the content.
1. Click on Cancel button will not delete the news .
1. You can also delete from the edit page, delete option is shown at the bottom of the edit page.

## Create draft news in EPC

1. login to the site.
1. Click Add content in a top shortcut menu.
1. Click on News link.
1. Click drop down arrow beside Save and keep published button in the bottom of a page.
1. The draft publication will be created by selecting Save and unpublished option in a drop down menu . 

## Set news date in EPC

1. login to the site.
1. Click Add content in the top shortcut menu.
1. Click on News link.
1. Click Authoring Information tab shown at the right side of the page and write date in Authored on field. The date format is YYYY-MM-DD and +0500 is the time zone offset from UTC. Leave blank to use the time of form submission.

## Set news author in EPC

1.	login to the site.
1.	Click  Add Content in the top shortcut menu..
1.	Click on News link.
1.	Click Authoring Information tab shown at the right side of the page and fill Authored By field (Leave blank for Anonymous).

## Translate a News

1. Login to the site.
1. Click News in top shortcut menu.
1. Scroll down to find the desired news.
1. Click drop down arrow beside Edit button.
1. Click Translate in drop down menu.
1. Enter the translation of title, body and other required fields.
1. Click Save.

# Rules for custom URLs

To add, edit URL, click on URL path settings from the right side tabs. Use the following rules to write URLs:

1. Use a relative path.
1. Use only lowercase English alphabets from a-z.
1. All URLs must start from either “publication/” or “news/” or “event/” or “media/”.
1. You can also add numbers.
1. Use dash (-) for spaces.
1. Don't add a trailing slash or the URL alias won't work.
1. Do not add special characters.

Here are some good URLs examples:

* news/first-news-post
* news/second-news-post-in-2016

Here are some bad URLs examples:

* news-first-news-post
* news-second-news-post-in-2016

# Promote to Front Page

1. Click Promotion options tab shown at the right side of the page . There are different options to select:
1. Stick at top of list : The post is published and shown on the top of a list.
1. Promoted to front page: The post published is added on front page.


# Managing Job

## Add Job
1. Login to the site
1. Click Add Content in a top shortcut menu .
1. Click on job link.
1. Fill the entire field, require fields with asterisk (*) are compulsory.
1. Enter title of a job.
1. Enter summary of a job in a body field.
1. Select a date from  pop-up calendar in apply by field.
1. Enter Package  in  a  package field .
1. Enter Location in a location field.
1. Enter Job type in a job type field .

## Add Jobs via Shortcut menu

1. Login to the site .
1. Click Add Jobs in a top shortcut menu.
1. Fill the entire fields and require fields with asterik (*) are compulsory in an appeared window.
1. Click Save and publish option in a drop down menu which appeared on clicking an arrow beside save as unpublished in a bottom of a page.  


## Edit Job
1. Login to the site
1. Click Jobs in a top shortcut menu .
1. Scroll down to find the desired job.
1. Click Edit button Shown at the right corner of each content .
1. Edit fields of a selected news content.
1. Click Save 

## Delete Job:

1. login to the site.
1. Click jobs in a top shortcut menu .
1. Scroll down to find the desired job.
1. Click drop down arrow beside Edit button.
1. Click Delete in drop down menu.
1. Confirmation page will occur click on delete button will permanently delete the content.
1. Click on Cancel button will not delete the news .
1. You can also delete from the edit page, delete option is shown at the bottom of the edit page.

## Create draft Job in EPC

1. login to the site.
1. Click Add content in a top shortcut menu.
1. Click on Job link.
1. Click drop down arrow beside Save and keep published button in the bottom of a page.
1. The draft publication will be created by selecting Save and unpublished option in a drop down menu . 


## Set Job date in EPC

1. login to the site.
1. Click Add content in the top shortcut menu.
1. Click on Job link.
1. Click Authoring Information tab shown at the right side of the page and write date in Authored on field. The date format is YYYY-MM-DD and +0500 is the time zone offset from UTC. Leave blank to use the time of form submission.

## Set Job author in EPC

1.	login to the site.
1.	Click  Add Content in the top shortcut menu..
1.	Click on News link.
1.	Click Authoring Information tab shown at the right side of the page and fill Authored By field (Leave blank for Anonymous).

## Translate a Job

1. Login to the site.
1. Click job in top shortcut menu.
1. Scroll down to find the desired job.
1. Click drop down arrow beside Edit button.
1. Click Translate in drop down menu.
1. Enter the translation of title, body and other required fields.
1. Click Save.

# Rules for custom URLs

To add, edit URL, click on URL path settings from the below tabs. Use the following rules to write URLs:

1. Use a relative path.
1. Use only lowercase English alphabets from a-z.
1. All URLs must start from either “publication/” or “news/” or “event/” or “media/” or “job/”  .
1. You can also add numbers.
1. Use dash (-) for spaces.
1. Don't add a trailing slash or the URL alias won't work
1. Do not add special characters.

Here are some good URLs examples:

* job/ researcher-job-vacancy

Here are some bad URLs examples:

* job-researcher-job-vacancy

# Managing Media

## Add Media
1. Login to the site
1. Click Add Content in a top shortcut menu .
1. Click on media link.
1. Fill the entire field, require fields with asterisk (*) are compulsory.
1. Enter title of Media.
1. Select a Channel from a dropdown in a channel field.
1. Enter a media link in a Link field.
1. Select a Research project from a dropdown in a research project field.
1. Select an event from a dropdown in an event field.

## Add Media via Shortcut menu

1. Login to the site .
1. Click Add Media in a top shortcut menu.
1. Fill the entire fields and require fields with asterik (*) are compulsory in an appeared window.
1. Click Save and publish option in a drop down menu which appeared on clicking an arrow beside save as unpublished in a bottom of a page.  

## Edit Media
1. Login to the site
1. Click Media in a top shortcut menu .
1. Scroll down to find the desired media.
1. Click Edit button Shown at the right corner of each content .
1. Edit fields of a selected news content.
1. Click Save 

## Delete Media:

1. login to the site.
1. Click media in a top shortcut menu .
1. Scroll down to find the desired media.
1. Click drop down arrow beside Edit button.
1. Click Delete in drop down menu.
1. Confirmation page will occur click on delete button will permanently delete the content.
1. Click on Cancel button will not delete the news .
1. You can also delete from the edit page, delete option is shown at the bottom of the edit page.

## Create draft Media in EPC

1. login to the site.
1. Click Add content in a top shortcut menu.
1. Click on Media link.
1. Click drop down arrow beside Save and keep published button in the bottom of a page.
1. The draft publication will be created by selecting Save and unpublished option in a drop down menu . 

## Set Media date in EPC

1. login to the site.
1. Click Add content in the top shortcut menu.
1. Click on Media link.
1. Click Authoring Information tab shown at the right side of the page and write date in Authored on field. The date format is YYYY-MM-DD and +0500 is the time zone offset from UTC. Leave blank to use the time of form submission.

## Set Media author in EPC

1.	login to the site.
1.	Click  Add Content in the top shortcut menu..
1.	Click on Media link.
1.	Click Authoring Information tab shown at the right side of the page and fill Authored By field (Leave blank for Anonymous).

## Translate a Media

1. Login to the site.
1. Click Media in top shortcut menu.
1. Scroll down to find the desired media.
1. Click drop down arrow beside Edit button.
1. Click Translate in drop down menu.
1. Enter the translation of title, body and other required fields.
1. Click Save.

# Rules for custom URLs

To add, edit URL, click on URL path settings from the below tabs. Use the following rules to write URLs:

1. Use a relative path.
1. Use only lowercase English alphabets from a-z.
1. All URLs must start from either “publication/” or “news/” or “event/” or “media/” or “job/”  .
1. You can also add numbers.
1. Use dash (-) for spaces.
1. Don't add a trailing slash or the URL alias won't work
1. Do not add special characters.

Here are some good URLs examples:

* media/media-content

Here are some bad URLs examples:

* media-media-content

# Managing Event

# Add event

1. Login to the site
1. Click Add Content in a top shortcut menu .
1. Click on event link.
1. Fill the entire field, require fields with asterisk (*) are compulsory.
1. Enter title of an event 
1. Select a date from  pop-up calendar in Start Date field. 
1. Select a date from  pop-up calendar in End date field.
1. Select an event type from a drop down in an event type field. 
1. Select an event venue from a drop down in an event venue field. 

## Manage  Event Overview
1. Click on Overview closed tab link.
1. Enter summary of an event in a body field.
1. Browse summary report choose file of  type .txt, .doc, .pdf, .docx and file must be less than 2 MB.

## Manage Event Registration
1. Click on Registration closed tab link.
1. Select an event reg type from a drop down in an event reg type field.
1. Enter seat count for an event.
1. Enter Contact person for an event.
1. Browse Brochure choose file of  type .txt, .doc, .pdf, .docx and file must be less than 2 MB.
1. Enter registration Link in an Auto complete field.

## Manage Event Image
1. Click on Images closed tab link.
1. Browse event images choose file of images. They should be with type .png, .gif, .jpg, .jpeg and file must be less than 2 MB.
1. Browse event qr image choose file of images. They should be with type .png, .gif, .jpg, .jpeg and file must be less than 2 MB.
1. Browse event app qr image choose file of images. They should be with type .png, .gif, .jpg, .jpeg and file must be less than 2 MB.



## Manage Event Agenda
1. Click on Agenda closed tab link.
1. Browse event agenda details choose file of  type .txt, .doc, .pdf, .docx and file must be less than 2 MB.

### Manage Event Activity
1. Click on Agenda closed tab link.
1. Select start date from  pop-up calendar in Start Date field. 
1. Select end date from  pop-up calendar in End date field.
1. Select Yes , No or N/A check box to show the Highlight of an event .
1. Enter the description of an event activity in a description field.

##### Add Event Activity
1. Click on Agenda closed tab link.
1. Click on the Add another item button to add an event activity.

##### Remove Event Activity
1. Click on Agenda closed tab link.
1. Click on the Remove button to remove an event activity.

## Manage Event Partners
1. Click on Partners closed tab link .
1. Select the Partners checkbox to show the partners of an event .

## Manage Event Highlights
1. Click on Highlights closed tab link.
1. Enter the label of an event highlight in a label field. 
1. Enter the style class of an event highlight in a style class field.
1. Enter the link of an event highlight in an auto-complete Link field.
1. Enter the description of an event activity in a description field.

### Add Event Highlights
1. Click on Highlights closed tab link.
1. Click on the Add another item button to add an event highlight.

### Remove Event Highlights
1. Click on Highlights closed tab link.
1. Click on the Remove button to remove an event highlight.


## Manage Event Venue
1. Click on Venue closed tab link.
1. Select Yes or No check box to show the Event venue details .
1. Enter the description about the date and time of an event in an Event details (When) field .
1. Enter the description about the location of an event in an Event details (Where) field .
1. Select Yes or No check box to show the location map of an event .

## Manage Event Research unit
1. Click on Research unit closed tab link.
1. Select the Research project from a drop down list in a research project field .

## Manage ADSD mobile App logo
1. Click on Mobile closed tab link.
1. Select Yes or No check box to show the Mobile app.
1. Enter an ADSD Mobile Applications link in an Auto complete field.

## Manage Links
1. Click on Links closed tab link.
1. Enter Press Releases link in an  Press Releases (link) Auto complete field.
1. Enter an ADSD Gallery link in an ADSD Gallery (link) Auto complete field.
1. Enter an ADSD Videos link in an  ADSD Videos(link)  Auto complete field.
1. Enter an ADSD twitter link in an ADSD twitter (link) Auto complete field.
1. Enter an ADSD Speeches link in an ADSD Speeches (link) Auto complete field.
1. Enter an ADSD in the Media link in an ADSD in the Media (link) Auto complete field.


## Manage Event Speeches

### Add Event speeches file
1. Click on Speeches closed tab link.
1. Browse Keynotes choose file of  type .txt, .doc, .pdf, .docx and file must be less than 2 MB.
1. Enter the description of an event speech file .

### Remove Event speeches file
1. Click on Speeches closed tab link.
1. Click on the Remove button to remove an event highlight.


## Add Event via Shortcut menu

1. Login to the site .
1. Click Add Event in a top shortcut menu.
1. Fill the entire fields and require fields with asterik (*) are compulsory in an appeared window.
1. Click Save and publish option in a drop down menu which appeared on clicking an arrow beside save as unpublished in a bottom of a page.  


## Edit Event
1. Login to the site
1. Click Event in a top shortcut menu .
1. Scroll down to find the desired event.
1. Click Edit button Shown at the right corner of each content .
1. Follow the same procedure  to edit the event as shown above for adding the event .
1. Click Save 

## Delete Event:

1. login to the site.
1. Click event in a top shortcut menu .
1. Scroll down to find the desired event.
1. Click drop down arrow beside Edit button.
1. Click Delete in drop down menu.
1. Confirmation page will occur click on delete button will permanently delete the content.
1. Click on Cancel button will not delete the news .
1. You can also delete from the edit page, delete option is shown at the bottom of the edit page.

## Create draft Event in EPC

1. login to the site.
1. Click Add content in a top shortcut menu.
1. Click on Event link.
1. Click drop down arrow beside Save and keep published button in the bottom of a page.
1. The draft publication will be created by selecting Save and unpublished option in a drop down menu . 

## Set Event date in EPC

1. login to the site.
1. Click Add content in the top shortcut menu.
1. Click on Event link.
1. Click Authoring Information tab shown at the right side of the page and write date in Authored on field. The date format is YYYY-MM-DD and +0500 is the time zone offset from UTC. Leave blank to use the time of form submission.

## Set Event author in EPC

1.	login to the site.
1.	Click  Add Content in the top shortcut menu..
1.	Click on Event link.
1.	Click Authoring Information tab shown at the right side of the page and fill Authored By field (Leave blank for Anonymous).

## Translate a Event

1. Login to the site.
1. Click Event in top shortcut menu.
1. Scroll down to find the desired event.
1. Click drop down arrow beside Edit button.
1. Click Translate in drop down menu.
1. Enter the translation of title, body and other required fields.
1. Click Save.

# Rules for custom URLs

To add, edit URL, click on URL path settings from the below tabs. Use the following rules to write URLs:

1. Use a relative path.
1. Use only lowercase English alphabets from a-z.
1. All URLs must start from either “publication/” or “news/” or “event/” or “media/” or “job/”.
1. You can also add numbers.
1. Use dash (-) for spaces.
1. Don't add a trailing slash or the URL alias won't work
1. Do not add special characters.

Here are some good URLs examples:

* event/this-is-my-second-event-held-in-2016

Here are some bad URLs examples:

* event-this-is-my-second-event-held-in-2016



# Managing Blog

## Add Blog
1. Login to the site
1. Click Add Content in a top shortcut menu .
1. Click on Blog link.
1. Fill the entire field, require fields with asterisk (*) are compulsory.
1. Enter title of Blog.
1. Enter summary of Blog in a body field.

## Add Blog via Shortcut menu

1. Login to the site .
1. Click Add Blog in a top shortcut menu.
1. Fill the entire fields and require fields with asterik (*) are compulsory in an appeared window.
1. Click Save and publish option in a drop down menu which appeared on clicking an arrow beside save as unpublished in a bottom of a page.  

## Edit Blog
1. Login to the site
1. Click blog in a top shortcut menu .
1. Scroll down to find the desired blog.
1. Click Edit button Shown at the right corner of each content .
1. Edit fields of a selected news content.
1. Click Save 

## Delete Blog:

1. login to the site.
1. Click blog in a top shortcut menu .
1. Scroll down to find the desired blog.
1. Click drop down arrow beside Edit button.
1. Click Delete in drop down menu.
1. Confirmation page will occur click on delete button will permanently delete the content.
1. Click on Cancel button will not delete the news .
1. You can also delete from the edit page, delete option is shown at the bottom of the edit page.

## Create draft Blog in EPC

1. login to the site.
1. Click Add content in a top shortcut menu.
1. Click on blog link.
1. Click drop down arrow beside Save and keep published button in the bottom of a page.
1. The draft publication will be created by selecting Save and unpublished option in a drop down menu . 

## Set blog date in EPC

1. login to the site.
1. Click Add content in the top shortcut menu.
1. Click on Blog link.
1. Click Authoring Information tab shown at the right side of the page and write date in Authored on field. The date format is YYYY-MM-DD and +0500 is the time zone offset from UTC. Leave blank to use the time of form submission.

## Set Blog author in EPC

1.	login to the site.
1.	Click  Add Content in the top shortcut menu..
1.	Click on blog link.
1.	Click Authoring Information tab shown at the right side of the page and fill Authored By field (Leave blank for Anonymous).






# Manage Publication Categories


## Add Publication type via Shortcut menu 

1. login to the site.
1. Click Publication type in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.


## Add Publisher via Shortcut menu 

1. login to the site.
1. Click Publisher in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page. 


## Add Publication group via Shortcut menu 

1. login to the site.
1. Click Publication group in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page. 


## Add Publication format via Shortcut menu 

1. login to the site.
1. Click Publication format in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page. 


## Add Research unit via Shortcut menu 

1. login to the site.
1. Click Research unit in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.






# Manage Event Categories



## Add  Event venue via Shortcut menu 

1. login to the site.
1. Click Venue in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.


## Add  Event type via Shortcut menu 

1. login to the site.
1. Click Event type in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.

## Add  Event reg type via Shortcut menu 

1. login to the site.
1. Click Event registration type in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.





# Manage News Categories


## Add Channel via Shortcut menu 

1. login to the site.
1. Click Channel in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.


## Add Research project via Shortcut menu 

1. login to the site.
1. Click Research project in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.
 
 
 
 
 
 
# Manage Media Categories


## Add Channel via Shortcut menu 

1. login to the site.
1. Click Channel in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.


## Add Research project via Shortcut menu 

1. login to the site.
1. Click Research project in a shortcut menu.
1. Add entire fields and required fields with (*) are compulsory in an appeared window.
1. Click Save on the bottom of a page.  






#Manage Default Categories


## Add Author via Shortcut menu 

1. login to the site.
1. Click Authors in a shortcut menu.
1. Click Add user button on the top left corner in an appeared window .
1. Add entire fields and required fields with (*) are compulsory.
1. Click Create new acount on the bottom of a page. 

 
## Check Audit log via Shortcut menu

1. login to the site.
1. Click Audit log in a shortcut menu.
1. Check the latest process running in a log file with a relevant information.
1. Click clear log mesages button present in a Clear log messages tab to clear the caches of a log file.
1. Select the filter information from a drop down and click Filter button present in a Filter log messages tab to filter the information present in a log file.




# Manage Front page

##  Manage English version via Shortcut menu
1. login to the site.
1. Click Front Carousel in a shortcut menu.
1. Select the type of content that have to be modified.

### Manage Events
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.



### Manage Publication - Books
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.



### Manage Publication - Papers
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.



### Manage News
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.






##  Manage Arabic version via Shortcut menu
1. login to the site.
1. Click Front Carousel(Arabic) in a shortcut menu.
1. Select the type of content that have to be modified.


### Manage Events
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.



### Manage Publication - Books
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.



### Manage Publication - Papers
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.



### Manage News
1. Select the region in which the content had to be modiifed. 

####  Add Content in Big box region
1. Select the content title from the drop down having label name as Big box.
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Top right box .
1. Click on Save Configurations to save the changes.

####  Add Content in right box region
1. Select the content title from the drop down having label name as Bottom right box .
1. Click on Save Configurations to save the changes.


